# ⚙ HTML, CSS and SASS Boilerplate 
This project is used as a boilerplate for tasks in the "HTML, CSS and SASS" course in boom.dev

🤯💥💣
# A JavaScript Task
After we have practiced intalling and uninstalling modules, now we have to update the version of **Anime.js** to the latest one. Then we have to implement an animation which is added in the latest versions of the library. 
## Objective
* Checkout the dev branch.
* Update to the latest version of **anime.js** which is **v3.2.1**
* Attach an event listener to the provided article element and it should listen for a click event
* Use anime.js to animate the article element when its clicked and its mandatory to use the **easing** property **spring**.
* When implemented merge the dev branch into master.


## Requirements
* The project starts with **npm run start**.

##Resources 
You can learn more about easing animations here - https://animejs.com/documentation/#springPhysicsEasing
 
